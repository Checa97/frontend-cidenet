import { IEmployee } from "../Components/create-employee/Iemployee";
import { IEmployeeDB } from "../Components/employee/IEmployeeDB";

export const validateEmail = (form: IEmployee, emails: IEmployeeDB[]): string => {
    const domain: string = form.job_country === "COL" ? "cidenet.com.co" : "cidenet.com.us";
    const random = Math.round(Math.random() * (100 - 1) + 1);
    let emailCreated = `${form.first_name.replace(/\s+/g, '').toLocaleLowerCase()}.${form.last_name.replace(/\s+/g, '').toLocaleLowerCase()}@${domain}`;

    for(let i = 0; i < emails.length; i++){
        if(emailCreated === emails[i].email){
            return `${form.first_name.replace(/\s+/g, '').toLocaleLowerCase()}.${form.last_name.replace(/\s+/g, '').toLocaleLowerCase()}.${random}@${domain}`;
        }
    }
    return emailCreated;
}