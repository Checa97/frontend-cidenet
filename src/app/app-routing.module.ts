import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEmployeeComponent } from './Components/create-employee/create-employee.component';
import { EmployeeComponent } from './Components/employee/employee.component';

const routes: Routes = [
  {
    path: 'employees',
    component: EmployeeComponent,
  },
  {
    path: 'employee',
    component: CreateEmployeeComponent
  },
  {
    path: 'employee/:id',
    component: CreateEmployeeComponent
  },
  {
    path: '**',
    component: EmployeeComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
