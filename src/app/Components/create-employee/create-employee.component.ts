import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { validateEmail } from 'src/app/helpers/validateEmail';
import { EmployeeService } from 'src/app/Services/employee.service';
import { IEmployeeDB } from '../employee/IEmployeeDB';
import { IEmployee } from './Iemployee';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  // Properties
  form: FormGroup;
  subRoute: Subscription | undefined;
  error: boolean = false;
  exist: boolean = false;

  id: string = "";
  deafultSelect: string = "";
  employee: IEmployeeDB = {
    id: 0,
    first_name: '',
    last_name: '',
    job_country: '',
    email: ''
  };
  emails: any = [];

  // Constructor
  constructor(
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    // Initialize reactive form
    this.form = this.formBuilder.group({
      first_name: ['', [Validators.required, Validators.maxLength(20)]],
      other_name: ['', [Validators.maxLength(50)]],
      last_name: ['', [Validators.required, Validators.maxLength(20)]],
      job_country: ['', Validators.required],
      email: [''],
    });    
  }

  // Get the id parameter for edit employee
  ngOnInit(): void {
    this.subRoute = this.route.params.subscribe(params => {
      this.id = params['id'];
      if (this.id !== undefined) {
        this.employeeService.getEmployee(this.id)
          .subscribe((data: IEmployeeDB) => {
            this.employee = data;
            this.deafultSelect = data.job_country;
          });
      }
    });

    this.getAllEmails();
  }

  ngOnDestroy() {
    this.subRoute?.unsubscribe();
  }

  // Validate and send Information to backend
  onSubmit() {
    if (this.form.valid) {
      const formValues: IEmployee = this.form.value;

      // Find the employee
      formValues.email = validateEmail(formValues, this.emails); // Create email

      // Create or Update
      if (this.id === undefined) {
        this.employeeService.createEmployee(formValues)
          .subscribe(() => {
            this.router.navigate(['/employees']);
          });
      } else {
        this.employeeService.updateEmployee(+this.id, formValues)
          .subscribe(() => {
            this.router.navigate(['/employees']);
          });
      }
    } else {
      this.error = true;
      console.log('Llene todos los campos');
    }
  }

  getAllEmails() {
    this.employeeService.getEmployees()
      .subscribe((data: IEmployeeDB[]) => {
        this.emails = data;
      });
  }

}
