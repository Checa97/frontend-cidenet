export interface IEmployeeDB {
    id: number;
    first_name: string;
    other_name?: string;
    last_name: string;
    job_country: string;
    email: string
  }