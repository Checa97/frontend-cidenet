import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/Services/employee.service';
import { SimpleModalService } from 'ngx-simple-modal';
import { ModalComponent } from '../modal/modal.component';
import { IEmployeeDB } from './IEmployeeDB';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {

  // List of employees from database
  public employees: IEmployeeDB[] = [];
  // Number of pagination
  public page!: number;

  constructor(private employeeService: EmployeeService, private modalService: SimpleModalService) {
    this.getAllEmployees();
  }

  ngOnInit(): void { }
  
  // Show delete option modal
  showComfirm(id: string) {
    let disposable: Subscription = this.modalService.addModal(ModalComponent)
      .subscribe((isComfirmed) => {
        if (isComfirmed) {
          this.employeeService.deleteEmployee(id)
            .subscribe(() => {
              this.getAllEmployees();
            });
        }
      });

    setTimeout(() => {
      disposable.unsubscribe();
    }, 10000)
  }
  
  // Get all Employees from database
  getAllEmployees() {
    this.employeeService.getEmployees()
      .subscribe((data: IEmployeeDB[]) => {
        this.employees = data;
      });
  }

}
