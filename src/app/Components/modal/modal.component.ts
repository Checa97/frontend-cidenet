import { Component, OnInit } from '@angular/core';
import { SimpleModalComponent } from "ngx-simple-modal";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent extends SimpleModalComponent<ModalComponent, boolean> implements OnInit {

  // Condition for open or close modal
  open: boolean = false;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  //Open or close modal
  confirm() {
    this.result = true;
    this.close();
  }

}
