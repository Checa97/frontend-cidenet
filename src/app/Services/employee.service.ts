import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IEmployee } from '../Components/create-employee/Iemployee';
import { IEmployeeDB } from '../Components/employee/IEmployeeDB';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private url: string = 'http://127.0.0.1:8000/api/employees/';

  constructor(private http: HttpClient) { }

  // Get All Employees
  getEmployees(): Observable<IEmployeeDB[]>{
    return this.http.get<IEmployeeDB[]>(this.url);
  }

  // Get Employee by ID
  getEmployee(id: string): Observable<IEmployeeDB>{
    return this.http.get<IEmployeeDB>(`${this.url}${id}/`);
  }
  
  // Create Employee
  createEmployee(employee: IEmployee){
    return this.http.post(this.url, employee);
  }

  // Update Employee
  updateEmployee(id: number, newEmployee: IEmployee){
    return this.http.put(`${this.url}${id}/`, newEmployee);
  }

  deleteEmployee(id: string){
    return this.http.delete(`${this.url}${id}/`);
  }
}
